
node[:mysql] ||= {}
node[:mysql][:conf] ||= {}
node[:mysql][:system] ||= {}

case node['platform']
when 'freebsd' then
  node[:mysql][:system][:package] ||= 'mariadb55-server'
  node[:mysql][:system][:service] ||= 'mysql-server'
when 'redhat', 'amazon' then
  node[:mysql][:system][:package] ||= 'mariadb-server'
  node[:mysql][:system][:service] ||= 'mariadb'
else
  node[:mysql][:system][:package]  ||= 'mariadb'
  node[:mysql][:system][:service]  ||= 'mariadb'
end


