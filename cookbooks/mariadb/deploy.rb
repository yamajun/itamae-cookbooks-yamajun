#
# CREATE DATABASE
#

include_recipe 'default'

if defined?(node[:mysql][:database]) and !node[:mysql][:database].nil?
  node[:mysql][:database].each do |db_name, config_db|

    sql_file_path = File.join '/tmp', 'mysql_initializer.sql'

    file "Create database initailizer for #{db_name}" do
      action :create
      sql = "-- Database/account initializer.\n"
      sql += "CREATE DATABASE #{db_name};\n"

      if defined?(config_db[:owner]) and !config_db[:owner].nil? \
        and defined?(config_db[:password]) and !config_db[:password].nil?
        sql += "GRANT ALL ON #{db_name}.* TO #{config_db[:owner]}@localhost IDENTIFIED BY '#{config_db[:password]}';\n"
      end

      content sql
      mode '0600'
      path sql_file_path
      not_if "echo 'SHOW DATABASES' | mysql --skip-column-names | grep '^#{db_name}$'"
    end

    execute "Create database: #{db_name}" do
      command "mysql < #{sql_file_path}"
      not_if "echo 'SHOW DATABASES' | mysql --skip-column-names | grep '^#{db_name}$'"
    end

    file "Remove Database initializer" do
      action :delete
      path sql_file_path
    end

    unless config_db[:sql_file].nil? or !config_db[:sql_file].kind_of? Array
      sql_file_path = File.join '/tmp', "mariadb-#{db_name}-init.sql"

      file "Put SQL initializer" do
        # すべての初期化用 SQL ファイルを結合して、一度で処理が終わるようにする。
        # そうしないと、テーブル作成済みチェックによる冪等性確保ができない。
        sql_string = ""
        for sql_file in config_db[:sql_file]

          # File#open では作業者のカレントディレクトリーを基準に
          # SQL ファイルを探索するため、レシピと違うパスにいるとエラーになる。
          sql_file_local_path = File.join File.dirname(__FILE__), sql_file

          File.open(sql_file_local_path, 'r') { |f|
            # 安全のために改行は入れておく。
            # 末尾に改行文字があったり、最後の行がコメントではないことを
            # 期待してはいけない。
            sql_string += "\r\n" + f.read
          }
        end

        content sql_string
        path sql_file_path
        mode "0644"

        # デプロイが終了していたら作成を行わない。
        only_if "echo \"SELECT COUNT(*) FROM information_schema.tables WHERE table_schema='#{db_name}';\" | mysql --skip-column-names | grep '^0$'"
      end

      execute "SQL table depoyment for: " + db_name do
        mysql_options = ''
        mysql_options += " -u '#{config_db[:owner]}'" unless config_db[:owner].nil?
        mysql_options += " -p'#{config_db[:password]}'" unless config_db[:password].nil?

        command "mysql #{mysql_options} < '#{sql_file_path}' '#{db_name}'"
        # FIXME Cannot check with SQL file nothing "CREATE TABLE".
        only_if "echo \"SELECT COUNT(*) FROM information_schema.tables WHERE table_schema='#{db_name}';\" | mysql --skip-column-names | grep '^0$'"
      end

      file 'Remove SQL initializer' do
        action :delete
        path sql_file_path
      end
    end

  end
end

