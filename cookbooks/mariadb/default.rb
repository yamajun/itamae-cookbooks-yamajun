#
# MariaDB(MySQL fork)
#

include_recipe 'attributes'

# Insetall server package
package node[:mysql][:system][:package]

service "Start and enable MariaDB daemon" do
  action [:enable, :start]
  name node[:mysql][:system][:service]
end

