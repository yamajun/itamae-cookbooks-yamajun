#
# Install, enable, start and deployment PostgreSQL.
#

include_recipe 'install'
include_recipe 'service'
include_recipe 'deploy'

