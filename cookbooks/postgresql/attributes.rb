#
# Attribute for postgresql.conf
# Based on postgresql-9.2.15
#

node[:postgresql] ||= {}
node[:postgresql][:conf] ||= {}
node[:postgresql][:system] ||= {}

#node[:postgresql][:conf][:data_directory] ||= "'ConfigDir'"
#node[:postgresql][:conf][:hba_file] ||= "'ConfigDir/pg_hba.conf'"
#node[:postgresql][:conf][:ident_file] ||= "'ConfigDir/pg_ident.conf'"
#node[:postgresql][:conf][:external_pid_file] ||= "''"


#node[:postgresql][:conf][:listen_addresses] ||= "'localhost'"
#node[:postgresql][:conf][:port] ||= 5432
node[:postgresql][:conf][:max_connections] ||= 100
#node[:postgresql][:conf][:superuser_reserved_connections] ||= 3
#node[:postgresql][:conf][:unix_socket_directories] ||= "'/var/run/postgresql, /tmp'"
#node[:postgresql][:conf][:unix_socket_group] ||= "''"
#node[:postgresql][:conf][:unix_socket_permissions] ||= "0777"
#node[:postgresql][:conf][:bonjour] ||= "off"
#node[:postgresql][:conf][:bonjour_name] ||= "''"

#node[:postgresql][:conf][:authentication_timeout] ||= "1min"
#node[:postgresql][:conf][:ssl] ||= "off"
#node[:postgresql][:conf][:ssl_ciphers] ||= "'ALL:!ADH:!LOW:!EXP:!MD5:@STRENGTH'"
#node[:postgresql][:conf][:ssl_renegotiation_limit] ||= 0
#node[:postgresql][:conf][:ssl_cert_file] ||= "'server.crt'"
#node[:postgresql][:conf][:ssl_key_file] ||= "'server.key'"
#node[:postgresql][:conf][:ssl_ca_file] ||= "''"
#node[:postgresql][:conf][:ssl_crl_file] ||= "''"
#node[:postgresql][:conf][:password_encryption] ||= "on"
#node[:postgresql][:conf][:db_user_namespace] ||= "off"

#node[:postgresql][:conf][:krb_server_keyfile] ||= "''"
#node[:postgresql][:conf][:krb_srvname] ||= "'postgres'"
#node[:postgresql][:conf][:krb_caseins_users] ||= "off"

#node[:postgresql][:conf][:tcp_keepalives_idle] ||= 0
#node[:postgresql][:conf][:tcp_keepalives_interval] ||= 0
#node[:postgresql][:conf][:tcp_keepalives_count] ||= 0


node[:postgresql][:conf][:shared_buffers] ||= "32MB"
#node[:postgresql][:conf][:temp_buffers] ||= "8MB"
#node[:postgresql][:conf][:max_prepared_transactions] ||= 0
#node[:postgresql][:conf][:work_mem] ||= "1MB"
#node[:postgresql][:conf][:maintenance_work_mem] ||= "16MB"
#node[:postgresql][:conf][:max_stack_depth] ||= "2MB"

#node[:postgresql][:conf][:temp_file_limit] ||= "-1"

#node[:postgresql][:conf][:max_files_per_process] ||= 1000
#node[:postgresql][:conf][:shared_preload_libraries] ||= "''"

#node[:postgresql][:conf][:vacuum_cost_delay] ||= "0ms"
#node[:postgresql][:conf][:vacuum_cost_page_hit] ||= 1
#node[:postgresql][:conf][:vacuum_cost_page_miss] ||= 10
#node[:postgresql][:conf][:vacuum_cost_page_dirty] ||= 20	
#node[:postgresql][:conf][:vacuum_cost_limit] ||= 200

#node[:postgresql][:conf][:bgwriter_delay] ||= "200ms"
#node[:postgresql][:conf][:bgwriter_lru_maxpages] ||= 100
#node[:postgresql][:conf][:bgwriter_lru_multiplier] ||= "2.0"

#node[:postgresql][:conf][:effective_io_concurrency] ||= 1


#node[:postgresql][:conf][:wal_level] ||= "minimal"
#node[:postgresql][:conf][:fsync] ||= "on"
#node[:postgresql][:conf][:synchronous_commit] ||= "on"
#node[:postgresql][:conf][:wal_sync_method] ||= "fsync"
#node[:postgresql][:conf][:full_page_writes] ||= "on"
#node[:postgresql][:conf][:wal_buffers] ||= "-1"
#node[:postgresql][:conf][:wal_writer_delay] ||= "200ms"

#node[:postgresql][:conf][:commit_delay] ||= 0
#node[:postgresql][:conf][:commit_siblings] ||= 5

#node[:postgresql][:conf][:checkpoint_segments] ||= 3
#node[:postgresql][:conf][:checkpoint_timeout] ||= "5min"
#node[:postgresql][:conf][:checkpoint_completion_target] ||= "0.5"
#node[:postgresql][:conf][:checkpoint_warning] ||= "30s"

#node[:postgresql][:conf][:archive_mode] ||= "off"
#node[:postgresql][:conf][:archive_command] ||= "''"
#node[:postgresql][:conf][:archive_timeout] ||= 0


#node[:postgresql][:conf][:max_wal_senders] ||= 0
#node[:postgresql][:conf][:wal_keep_segments] ||= 0
#node[:postgresql][:conf][:replication_timeout] ||= "60s"

#node[:postgresql][:conf][:synchronous_standby_names] ||= "''"
#node[:postgresql][:conf][:vacuum_defer_cleanup_age] ||= 0

#node[:postgresql][:conf][:hot_standby] ||= "off"
#node[:postgresql][:conf][:max_standby_archive_delay] ||= "30s"
#node[:postgresql][:conf][:max_standby_streaming_delay] ||= "30s"
#node[:postgresql][:conf][:wal_receiver_status_interval] ||= "10s"
#node[:postgresql][:conf][:hot_standby_feedback] ||= "off"


#node[:postgresql][:conf][:enable_bitmapscan] ||= "on"
#node[:postgresql][:conf][:enable_hashagg] ||= "on"
#node[:postgresql][:conf][:enable_hashjoin] ||= "on"
#node[:postgresql][:conf][:enable_indexscan] ||= "on"
#node[:postgresql][:conf][:enable_indexonlyscan] ||= "on"
#node[:postgresql][:conf][:enable_material] ||= "on"
#node[:postgresql][:conf][:enable_mergejoin] ||= "on"
#node[:postgresql][:conf][:enable_nestloop] ||= "on"
#node[:postgresql][:conf][:enable_seqscan] ||= "on"
#node[:postgresql][:conf][:enable_sort] ||= "on"
#node[:postgresql][:conf][:enable_tidscan] ||= "on"

#node[:postgresql][:conf][:seq_page_cost] ||= "1.0"
#node[:postgresql][:conf][:random_page_cost] ||= "4.0"
#node[:postgresql][:conf][:cpu_tuple_cost] ||= "0.01"
#node[:postgresql][:conf][:cpu_index_tuple_cost] ||= "0.005"
#node[:postgresql][:conf][:cpu_operator_cost] ||= "0.0025"
#node[:postgresql][:conf][:effective_cache_size] ||= "128MB"

#node[:postgresql][:conf][:geqo] ||= "on"
#node[:postgresql][:conf][:geqo_threshold] ||= 12
#node[:postgresql][:conf][:geqo_effort] ||= 5
#node[:postgresql][:conf][:geqo_pool_size] ||= 0
#node[:postgresql][:conf][:geqo_generations] ||= 0
#node[:postgresql][:conf][:geqo_selection_bias] ||= "2.0"
#node[:postgresql][:conf][:geqo_seed] ||= "0.0"

#node[:postgresql][:conf][:default_statistics_target] ||= 100
#node[:postgresql][:conf][:constraint_exclusion] ||= "partition"
#node[:postgresql][:conf][:cursor_tuple_fraction] ||= "0.1"
#node[:postgresql][:conf][:from_collapse_limit] ||= 8
#node[:postgresql][:conf][:join_collapse_limit] ||= 8


#node[:postgresql][:conf][:log_destination] ||= "'stderr'"

node[:postgresql][:conf][:logging_collector] ||= "on"

#node[:postgresql][:conf][:log_directory] ||= "'pg_log'"
node[:postgresql][:conf][:log_filename] ||= "'postgresql-%a.log'"
#node[:postgresql][:conf][:log_file_mode] ||= "0600"
node[:postgresql][:conf][:log_truncate_on_rotation] ||= "on"
node[:postgresql][:conf][:log_rotation_age] ||= "1d"
node[:postgresql][:conf][:log_rotation_size] ||= 0

#node[:postgresql][:conf][:syslog_facility] ||= "'LOCAL0'"
#node[:postgresql][:conf][:syslog_ident] ||= "'postgres'"

#node[:postgresql][:conf][:event_source] ||= "'PostgreSQL'"

#node[:postgresql][:conf][:client_min_messages] ||= "notice"
#node[:postgresql][:conf][:log_min_messages] ||= "warning"
#node[:postgresql][:conf][:log_min_error_statement] ||= "error"
#node[:postgresql][:conf][:log_min_duration_statement] ||= "-1"

#node[:postgresql][:conf][:debug_print_parse] ||= "off"
#node[:postgresql][:conf][:debug_print_rewritten] ||= "off"
#node[:postgresql][:conf][:debug_print_plan] ||= "off"
#node[:postgresql][:conf][:debug_pretty_print] ||= "on"
#node[:postgresql][:conf][:log_checkpoints] ||= "off"
#node[:postgresql][:conf][:log_connections] ||= "off"
#node[:postgresql][:conf][:log_disconnections] ||= "off"
#node[:postgresql][:conf][:log_duration] ||= "off"
#node[:postgresql][:conf][:log_error_verbosity] ||= "default"
#node[:postgresql][:conf][:log_hostname] ||= "off"
#node[:postgresql][:conf][:log_line_prefix] ||= "''"
#node[:postgresql][:conf][:log_lock_waits] ||= "off"
#node[:postgresql][:conf][:log_statement] ||= "'none"'
#node[:postgresql][:conf][:log_temp_files] ||= "-1"
# https://www.postgresql.org/docs/9.2/static/runtime-config-logging.html#GUC-LOG-TIMEZONE
# https://www.postgresql.org/docs/9.2/static/datatype-datetime.html#DATATYPE-TIMEZONES
node[:postgresql][:conf][:log_timezone] ||= "'UTC'"


#node[:postgresql][:conf][:track_activities] ||= "on"
#node[:postgresql][:conf][:track_counts] ||= "on"
#node[:postgresql][:conf][:track_io_timing] ||= "off"
#node[:postgresql][:conf][:track_functions] ||= "none"
#node[:postgresql][:conf][:track_activity_query_size] ||= 1024
#node[:postgresql][:conf][:update_process_title] ||= "on"
#node[:postgresql][:conf][:stats_temp_directory] ||= "'pg_stat_tmp'"

#node[:postgresql][:conf][:log_parser_stats] ||= "off"
#node[:postgresql][:conf][:log_planner_stats] ||= "off"
#node[:postgresql][:conf][:log_executor_stats] ||= "off"
#node[:postgresql][:conf][:log_statement_stats] ||= "off"


#node[:postgresql][:conf][:autovacuum] ||= "on"
#node[:postgresql][:conf][:log_autovacuum_min_duration] ||= "-1"
#node[:postgresql][:conf][:autovacuum_max_workers] ||= 3
#node[:postgresql][:conf][:autovacuum_naptime] ||= "1min"
#node[:postgresql][:conf][:autovacuum_vacuum_threshold] ||= 50
#node[:postgresql][:conf][:autovacuum_analyze_threshold] ||= 50
#node[:postgresql][:conf][:autovacuum_vacuum_scale_factor] ||= "0.2"
#node[:postgresql][:conf][:autovacuum_analyze_scale_factor] ||= "0.1"
#node[:postgresql][:conf][:autovacuum_freeze_max_age] ||= "200000000"
#node[:postgresql][:conf][:autovacuum_vacuum_cost_delay] ||= "20ms"
#node[:postgresql][:conf][:autovacuum_vacuum_cost_limit] ||= "-1"


#node[:postgresql][:conf][:search_path] ||= '\'"$user",public\''
#node[:postgresql][:conf][:default_tablespace] ||= "''"
#node[:postgresql][:conf][:temp_tablespaces] ||= "''"
#node[:postgresql][:conf][:check_function_bodies] ||= "on"
#node[:postgresql][:conf][:default_transaction_isolation] ||= "'read committed'"
#node[:postgresql][:conf][:default_transaction_read_only] ||= "off"
#node[:postgresql][:conf][:default_transaction_deferrable] ||= "off"
#node[:postgresql][:conf][:session_replication_role] ||= "'origin'"
#node[:postgresql][:conf][:statement_timeout] ||= 0
#node[:postgresql][:conf][:vacuum_freeze_min_age] ||= "50000000"
#node[:postgresql][:conf][:vacuum_freeze_table_age] ||= "150000000"
#node[:postgresql][:conf][:bytea_output] ||= "'hex'"
#node[:postgresql][:conf][:xmlbinary] ||= "'base64'"
#node[:postgresql][:conf][:xmloption] ||= "'content'"
#node[:postgresql][:conf][:gin_fuzzy_search_limit] ||= 0

node[:postgresql][:conf][:datestyle] ||= "'iso, mdy'"
#node[:postgresql][:conf][:intervalstyle] ||= "'postgres'"
# https://www.postgresql.org/docs/9.2/static/runtime-config-client.html#GUC-TIMEZONE
# http://stackoverflow.com/questions/6663765/postgres-default-timezone
node[:postgresql][:conf][:timezone] ||= "'UTC'"
#node[:postgresql][:conf][:timezone_abbreviations] ||= "'Default'"

#node[:postgresql][:conf][:extra_float_digits] ||= 0
#node[:postgresql][:conf][:client_encoding] ||= "sql_ascii"

node[:postgresql][:conf][:lc_messages] ||= "'C'"
node[:postgresql][:conf][:lc_monetary] ||= "'C'"
node[:postgresql][:conf][:lc_numeric] ||= "'C'"
node[:postgresql][:conf][:lc_time] ||= "'C'"

node[:postgresql][:conf][:default_text_search_config] ||= "'pg_catalog.english'"

#node[:postgresql][:conf][:dynamic_library_path] ||= '\'$libdir\''
#node[:postgresql][:conf][:local_preload_libraries] ||= "''"


#node[:postgresql][:conf][:deadlock_timeout] ||= "1s"
#node[:postgresql][:conf][:max_locks_per_transaction] ||= 64
#node[:postgresql][:conf][:max_pred_locks_per_transaction] ||= 64


#node[:postgresql][:conf][:array_nulls] ||= "on"
#node[:postgresql][:conf][:backslash_quote] ||= "safe_encoding"
#node[:postgresql][:conf][:default_with_oids] ||= "off"
#node[:postgresql][:conf][:escape_string_warning] ||= "on"
#node[:postgresql][:conf][:lo_compat_privileges] ||= "off"
#node[:postgresql][:conf][:quote_all_identifiers] ||= "off"
#node[:postgresql][:conf][:sql_inheritance] ||= "on"
#node[:postgresql][:conf][:standard_conforming_strings] ||= "on"
#node[:postgresql][:conf][:synchronize_seqscans] ||= "on"

#node[:postgresql][:conf][:transform_null_equals] ||= "off"


#node[:postgresql][:conf][:exit_on_error] ||= "off"
#node[:postgresql][:conf][:restart_after_crash] ||= "on"


case node['platform']
when 'redhat', 'amazon' then
  node[:postgresql][:system][:package]  ||= 'postgresql-server'
  node[:postgresql][:system][:service]  ||= 'postgresql'

  node[:postgresql][:system][:conf_dir] ||= '/var/lib/pgsql/data'
  node[:postgresql][:system][:user]  ||= 'postgres'
  node[:postgresql][:system][:group] ||= 'postgres'
  node[:postgresql][:system][:home] ||= '/var/lib/pgsql'

when 'freebsd' then
  node[:postgresql][:system][:package]  ||= 'postgresql95-server'
  node[:postgresql][:system][:service]  ||= 'postgresql'

  node[:postgresql][:system][:conf_dir] ||= '/usr/local/pgsql/data'
  node[:postgresql][:system][:user]  ||= 'pgsql'
  node[:postgresql][:system][:group] ||= 'pgsql'
  node[:postgresql][:system][:home] ||= '/usr/local/pgsql'

when 'debian', 'ubuntu' then
  node[:postgresql][:system][:package]  ||= 'postgresql'
  node[:postgresql][:system][:service]  ||= 'postgresql'

  node[:postgresql][:system][:conf_dir] ||= '/etc/postgresql/9.4/main'
  node[:postgresql][:system][:user]  ||= 'postgres'
  node[:postgresql][:system][:group] ||= 'postgres'
  node[:postgresql][:system][:home] ||= '/var/lib/postgresql'

else
  node[:postgresql][:system][:package]  ||= 'postgresql'
  node[:postgresql][:system][:service]  ||= 'postgresql'

  node[:postgresql][:system][:conf_dir] ||= '/usr/local/pgsql'
  node[:postgresql][:system][:user]  ||= 'postgres'
  node[:postgresql][:system][:group] ||= 'postgres'
  node[:postgresql][:system][:home] ||= '/usr/local/pgsql'
end

node[:postgresql][:system][:data_dir] ||= node[:postgresql][:system][:conf_dir] 

