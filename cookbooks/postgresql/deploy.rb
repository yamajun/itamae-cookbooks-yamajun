#
# Create PostgreSQL user(a.k.a. role) and database.
# Required: grep(1), Running PostgrSQL daemon.
#

include_recipe 'attributes'

if defined?(node[:postgresql][:user]) and !node[:postgresql][:user].nil?
  node[:postgresql][:user].each do |user_name, config_user|
    createuser_options = ''
    # SUPERUSER == CREATEUSER
    # https://www.postgresql.org/docs/9.2/static/sql-createrole.html
    createuser_options += " SUPERUSER"  if defined?(config_user[:superuser])  and config_user[:superuser]
    createuser_options += " CREATEDB"   if defined?(config_user[:createdb])   and config_user[:createdb]
    createuser_options += " CREATEROLE" if defined?(config_user[:createrole]) and config_user[:createrole]
    createuser_options += " REPLICATION" if defined?(config_user[:replication]) and config_user[:replication]
    createuser_options += " PASSWORD '#{config_user[:password]}'" unless config_user[:password].nil?

    execute "Create PostgreSQL user: " + user_name do
      user node[:postgresql][:system][:user]
      # https://www.postgresql.org/docs/9.2/static/sql-createuser.html
      command "echo \"CREATE USER \\\"#{user_name}\\\" #{createuser_options};\" | psql"

      not_if "echo \"SELECT * FROM pg_user WHERE usename='#{user_name}';\" | psql --tuples-only| grep '#{user_name}'"
    end

  end
end

if defined?(node[:postgresql][:database]) and !node[:postgresql][:database].nil?
  node[:postgresql][:database].each do |db_name, config_db|
    createdb_options = ''
    createdb_options += ' --owner='    + config_db[:owner]    unless config_db[:owner].nil?
    createdb_options += ' --locale='   + config_db[:locale]   unless config_db[:locale].nil?
    createdb_options += ' --encoding=' + config_db[:encoding] unless config_db[:encoding].nil?
    createdb_options += ' --template=' + config_db[:template] unless config_db[:template].nil?
    createdb_options += ' '

    execute "Create PostgreSQL database: " + db_name do
      user node[:postgresql][:system][:user]
      command "createdb" + createdb_options + db_name

      not_if "echo \"SELECT * FROM pg_database WHERE datname='#{db_name}';\" | psql --tuples-only | grep '#{db_name}'"
    end

    unless config_db[:sql_file].nil? or !config_db[:sql_file].kind_of? Array

      sql_file_path = File.join '/tmp', "pgsql-#{db_name}-init.sql"

      file "Put SQL initializer" do
        # すべての初期化用 SQL ファイルを結合して、一度で処理が終わるようにする。
        # そうしないと、テーブル作成済みチェックによる冪等性確保ができない。
        sql_string = ""
        for sql_file in config_db[:sql_file]

          # File#open では作業者のカレントディレクトリーを基準に
          # SQL ファイルを探索するため、レシピと違うパスにいるとエラーになる。
          sql_file_local_path = File.join File.dirname(__FILE__), sql_file

          File.open(sql_file_local_path, 'r') { |f|
            # 安全のために改行は入れておく。
            # 末尾に改行文字があったり、最後の行がコメントではないことを
            # 期待してはいけない。
            sql_string += "\r\n" + f.read
          }
        end

        content sql_string
        path sql_file_path
        mode "0644"

        # デプロイが終了していたら作成を行わない。
        not_if "echo \"\\d\" | psql --tuples-only '#{db_name}' | grep 'table'"
      end

      # NOTE If required password for your database.  Put .pgpass
      # at 'postgres' user's home directory.
      execute "SQL table depoyment for: " + db_name do
        user node[:postgresql][:system][:user]

        psql_options = ''
        psql_options += " -U '#{config_db[:owner]}'" unless config_db[:owner].nil?
        psql_options += " -h '#{config_db[:allowed_host]}'" unless config_db[:allowed_host].nil?

        command "psql #{psql_options} -f '#{sql_file_path}' '#{db_name}'"
        # FIXME Cannot check with SQL file nothing "CREATE TABLE".
        not_if "echo \"\\d\" | psql --tuples-only '#{db_name}' | grep 'table'"
      end

      file "Remove SQL initializer" do
        action :delete
        path sql_file_path
      end

    end

  end
end

