#
# Enable and start PostgreSQL service.
#

include_recipe 'attributes'

service 'Stert and enable PostgreSQL daemon' do
  action [:enable, :start]
  name node[:postgresql][:system][:service]
end

