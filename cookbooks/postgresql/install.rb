#
# Install PostgreSQL package from package system.
#

include_recipe 'attributes'

pgsql_init_option = "--encoding=UTF8"

package 'Install PostgreSQL daemon' do
  action :install
  name node[:postgresql][:system][:package]
end

execute "Initailize PostgreSQL data directory" do
  case node['platform']
  #when 'redhat', 'amazon' then
    # same with code at "else".
  when 'freebsd' then
    command "service #{node[:postgresql][:system][:package]} initdb"

  when 'debian', 'ubuntu' then
    command "service #{node[:postgresql][:system][:package]} initdb"
  else
    command "initdb #{pgsql_init_option} --pgdata #{node[:postgresql][:system][:data_dir]}"
    user node[:postgresql][:system][:user]
  end

  not_if "test -f #{node[:postgresql][:system][:conf_dir]}/postgresql.conf" 
end

template 'postgresql.conf' do
  owner node[:postgresql][:system][:user]
  group node[:postgresql][:system][:group]
  source 'templates/postgresql.conf.erb'
  path File.join(node[:postgresql][:system][:conf_dir], 'postgresql.conf')
end

template 'pg_hba.conf' do
  owner node[:postgresql][:system][:user]
  group node[:postgresql][:system][:group]
  source 'templates/pg_hba.conf.erb'
  path File.join(node[:postgresql][:system][:conf_dir], 'pg_hba.conf')
end

