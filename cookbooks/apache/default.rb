#
# ディストリビューションが提供する Apache httpd のパッケージをインストールする。
#

include_recipe 'attributes'

package 'Install Apache HTTP daemon' do
  action :install
  name node[:apache][:system][:package]
end

# 冪等性を確保しつつ、AllowOverride の設定値を変更し、
# .htaccess が Limit を設定できるようにする。
unless node[:apache][:system][:allow_override] == 'None'
  execute "Enable \"#{node[:apache][:system][:allow_override]}\" on DocumentRoot" do
    command "sed -i.orig -e '\\X<Directory \"#{node[:apache][:system][:document_root]}\">X,\\X</Directory>X" \
        + " s/^\\([[:space:]]*\\)AllowOverride .*$/\\1AllowOverride #{node[:apache][:system][:allow_override]}/' #{node[:apache][:system][:httpd_conf]}"

    # ファイルがないケース・設定済みケースを無視する。
    not_if "test ! -f #{node[:apache][:system][:httpd_conf]}" \
          + "|| grep 'AllowOverride #{node[:apache][:system][:allow_override]}' #{node[:apache][:system][:httpd_conf]}"
  end

  service 'Restart Apache HTTP daemon' do
    action :restart
    name node[:apache][:system][:service]
  end

end

service 'Stert and enable Apache HTTP daemon' do
  action [:enable, :start]
  name node[:apache][:system][:service]
end

