#
# Default attributes for apache
#

node[:apache] ||= {}
node[:apache][:system] ||= {}

case node[:platform]
when 'freebsd'
  node[:apache][:system][:package]  ||= 'apache24'
  node[:apache][:system][:service]  ||= 'apache24'
  node[:apache][:system][:user]     ||= 'www'
  node[:apache][:system][:group]    ||= 'www'
  node[:apache][:system][:httpd_conf]    ||= '/usr/local/etc/apache24/httpd.conf'
  node[:apache][:system][:document_root] ||= '/usr/local/www/apache24/data'

when 'debian', 'ubuntu' then
  node[:apache][:system][:package]  ||= 'apache2'
  node[:apache][:system][:service]  ||= 'apache2'
  node[:apache][:system][:user]     ||= 'www-data'
  node[:apache][:system][:group]    ||= 'www-data'
  node[:apache][:system][:httpd_conf]    ||= '/etc/apache2/apache2.conf'
  node[:apache][:system][:document_root] ||= '/var/www'

when 'redhat', 'amazon' then
  node[:apache][:system][:package]  ||= 'httpd'
  node[:apache][:system][:service]  ||= 'httpd'
  node[:apache][:system][:user]     ||= 'apache'
  node[:apache][:system][:group]    ||= 'apache'
  node[:apache][:system][:httpd_conf]    ||= '/etc/httpd/conf/httpd.conf'
  node[:apache][:system][:document_root] ||= '/var/www/html'

else
  node[:apache][:system][:package]  ||= 'apache'
  node[:apache][:system][:service]  ||= 'apache'
  node[:apache][:system][:user]     ||= 'apache'
  node[:apache][:system][:group]    ||= 'apache'
  node[:apache][:system][:httpd_conf]    ||= '/usr/local/apache/conf/httpd.conf'
  node[:apache][:system][:document_root] ||= '/usr/local/apache/www'
end

# AllowOverride value on DocumentRoot
# None: Not change
# AuthConfig Limit: Permit "Allow, Deny" in .htaccess
# Options: Permit "php_value" in .htaccess
node[:apache][:system][:allow_override] ||= 'None'

