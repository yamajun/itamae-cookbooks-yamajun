
include_recipe 'default'

unless node[:openldap][:ldif].nil? \
    or !node[:openldap][:ldif].kind_of? Array

  ldif_file_path = File.join '/tmp', "openldap-init.ldif"

  # すべての初期化用 LDIF ファイルを結合して一度で処理が終わるようにする。
  # そうしないと、データ数チェックによる冪等性確保ができない。
  file 'Create LDIF initializer' do
    ldif_string = ''
    for ldif_file in node[:openldap][:ldif]
      ldif_file_local_path = File.join File.dirname(__FILE__), ldif_file

      File.open(ldif_file_local_path, 'r') { |f|
        # 安全のために改行は入れておく。
        # 末尾に改行文字があったり、最後の行がコメントではないことを
        # 期待してはいけない。
        ldif_string += "\r\n" + f.read
      }
    end

    content ldif_string
    path ldif_file_path
    mode "0644"

    # デプロイが終了してツリー構造やデータが存在していたら作成を行わない。
    not_if "ldapsearch -x -D #{node[:openldap][:binddn]} -w '#{node[:openldap][:binddn_password]}' | grep numEntries"
  end

  execute "Import LDIF initializer" do
    command "ldapadd -x -D #{node[:openldap][:binddn]} -w '#{node[:openldap][:binddn_password]}' -f #{ldif_file_path}"
    not_if "ldapsearch -x -D #{node[:openldap][:binddn]} -w '#{node[:openldap][:binddn_password]}' | grep numEntries"
  end

  file 'Remove LDIF initializer' do
    action :delete
    path ldif_file_path
  end
end

