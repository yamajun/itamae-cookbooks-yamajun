
node[:openldap] ||= {}
node[:openldap][:system] ||= {}
node[:openldap][:system][:slapd_conf] ||= {}
node[:openldap][:system][:slapd_conf][:olc_suffix] ||= 'dc=my-domain,dc=com'
# "secret" を slappasswd(8) で変換したもの。
node[:openldap][:system][:slapd_conf][:olc_root_pw] ||= '{SSHA}RQSRGNkd1eVYdDwCie2HZ0TqWMlcE68/'
node[:openldap][:binddn] ||= 'cn=Manager,dc=my-domain,dc=com'
node[:openldap][:binddn_password] ||= 'secret'


case node[:platform]
when 'freebsd' then
  # openldap-server depends on openldap-client
  node[:openldap][:system][:packages]   ||= 'openldap-server'
  node[:openldap][:system][:service]    ||= 'slapd'
  node[:openldap][:system][:schema]     ||= '/usr/local/etc/openldap/schema'
  node[:openldap][:system][:slapd_conf][:path] ||= '/usr/local/etc/openldap/slapd.conf'
  # /etc/rc.conf{,.local} に slapd_cn_config="YES" が指定されている必要がある。
  node[:openldap][:system][:slapd_dir]  ||= '/usr/local/etc/openldap/slapd.d'
  node[:openldap][:system][:module_dir] ||= '/usr/local/libexec/openldap'
  node[:openldap][:system][:data_dir]   ||= '/var/db/openldap-data'
  node[:openldap][:system][:pid_dir]    ||= '/var/run/openldap'
  node[:openldap][:system][:user]       ||= 'ldap'
  node[:openldap][:system][:group]      ||= 'ldap'
when 'redhat', 'amazon' then
  node[:openldap][:system][:packages]   ||= 'openldap-servers openldap-clients'
  node[:openldap][:system][:service]    ||= 'slapd'
  node[:openldap][:system][:schema]     ||= '/etc/openldap/schema'
  node[:openldap][:system][:slapd_conf][:path] ||= 'slapd-config'
  node[:openldap][:system][:slapd_dir]  ||= '/etc/openldap/slapd.d'
  node[:openldap][:system][:module_dir] ||= '/usr/lib64/openldap'
  node[:openldap][:system][:data_dir]   ||= '/var/lib/ldap'
  node[:openldap][:system][:pid_dir]    ||= '/var/run/openldap'
  node[:openldap][:system][:user]       ||= 'ldap'
  node[:openldap][:system][:group]      ||= 'ldap'
else
  node[:openldap][:system][:packages]   ||= 'openldap'
  node[:openldap][:system][:service]    ||= 'slapd'
  node[:openldap][:system][:schema]     ||= '/usr/local/etc/openldap/schema'
  node[:openldap][:system][:slapd_conf][:path] ||= '/usr/local/etc/openldap/slapd.conf'
  node[:openldap][:system][:slapd_dir]  ||= '/usr/local/etc/openldap/slapd.d'
  node[:openldap][:system][:module_dir] ||= '/usr/local/libexec/openldap'
  node[:openldap][:system][:data_dir]   ||= '/var/openldap-data'
  node[:openldap][:system][:pid_dir]    ||= '/var/run'
  node[:openldap][:system][:user]       ||= 'ldap'
  node[:openldap][:system][:group]      ||= 'ldap'
end

