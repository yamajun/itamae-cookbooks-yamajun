
include_recipe 'attributes'

if node[:openldap][:system][:slapd_conf][:path] == 'slapd-config'

  # ldapmodify による編集を行うには、slapd の起動が必要。
  service 'Start OpenLDAP server' do
    action :start
    name node[:openldap][:system][:service]
  end

  # LDIF 初期化ファイルはパスワードなどの情報が含まれるため、
  # root 以外には読ませない。
  template '/tmp/00change_config_rootdn.ldif' do
    owner 'root'
    group '0'
    mode 'go-rwx'
    source 'templates/00change_config_rootdn.ldif.erb'
  end

  execute 'Initial root DN' do
    command 'ldapmodify -Y EXTERNAL -H ldapi:// -f /tmp/00change_config_rootdn.ldif && rm /tmp/00change_config_rootdn.ldif'
    not_if "test -f #{node[:openldap][:system][:slapd_dir]}/cn=config/cn=schema/cn*cosine.ldif"
  end

  unless node[:openldap][:system][:slapd_conf][:include_schema].nil? \
      or !node[:openldap][:system][:slapd_conf][:include_schema].kind_of? Array
    for schema in node[:openldap][:system][:slapd_conf][:include_schema]

      execute "Import OpenLDAP schema: #{schema}" do
        command "ldapadd -Y EXTERNAL -H ldapi:// -f #{node[:openldap][:system][:schema]}/#{schema}.ldif"
        not_if "test -f #{node[:openldap][:system][:slapd_dir]}/cn=config/cn=schema/cn*#{schema}.ldif"
      end

    end
  end

else
  # slapd-config(5) で設定しない場合には、slapd を使用する。

  slapd_conf_schemas ||= ''

  unless node[:openldap][:system][:slapd_conf][:include_schema].nil? \
      or !node[:openldap][:system][:slapd_conf][:include_schema].kind_of? Array
    for schema in node[:openldap][:system][:slapd_conf][:include_schema]

      slapd_conf_schemas += <<EOF
include		#{node[:openldap][:system][:schema]}/#{schema}.schema
EOF

    end
  end

  template node[:openldap][:system][:slapd_conf][:path] do
    source 'templates/slapd.conf.erb'
    owner 'root'
    group '0'
    mode '0600'
    variables slapd_conf_schemas: slapd_conf_schemas
  end
end

# slapd を変更後は再起動が必要だが、通常は起動前に行うので不要。
#service node[:openldap][:system][:service] do
#  action :restart
#end

