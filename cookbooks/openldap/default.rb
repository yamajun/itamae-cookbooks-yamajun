#
# このレシピではインストールと起動しか行わない。
# ツリーの初期化は init_nis_tree.rb を使うか、自分で定義すること。
#

include_recipe 'attributes'

package node[:openldap][:system][:packages]

include_recipe 'slapd_conf'

service 'Start OpenLDAP server' do
  action [:enable, :start]
  name node[:openldap][:system][:service]
end

