#!/bin/sh

if [ ! -f "$1" ]; then
	echo "usage: $0 /path/to/php.ini"
	exit 1
fi


cat -<<EOF
#
# Default attributes for PHP
#

node[:php] ||= {}
node[:php][:system] ||= {}
node[:php][:system][:ini] ||= {}
node[:php][:system][:ini][:mode] ||= '0644';

case node[:platform]
when 'freebsd'
  node[:php][:system][:ini][:path] ||= '/usr/local/etc/php.ini'
  node[:php][:system][:ini][:owner] ||= 'root';
  node[:php][:system][:ini][:group] ||= 'wheel';

when 'debian', 'ubuntu' then
  node[:php][:system][:ini][:path] ||= '/etc/php5/apache2/php.ini'
  #node[:php][:system][:ini][:path] ||= '/etc/php5/cli/php.ini'
  node[:php][:system][:ini][:owner] ||= 'root';
  node[:php][:system][:ini][:group] ||= 'root';

when 'redhat', 'amazon' then
  node[:php][:system][:ini][:path] ||= '/etc/php.ini'
  node[:php][:system][:ini][:owner] ||= 'root';
  node[:php][:system][:ini][:group] ||= 'root';

else
  node[:php][:system][:ini][:path] ||= '/usr/local/php/etc/php.ini'
  node[:php][:system][:ini][:owner] ||= 'root';
  node[:php][:system][:ini][:group] ||= 'root';
end

# If not defined, don't update php.ini
if defined? node[:php][:ini] and node[:php][:ini].kind_of? Hash

  # [PHP] Section
  node[:php][:ini][:user_ini] ||= {}
  node[:php][:ini][:zlib] ||= {}
  node[:php][:ini][:highlight] ||= {}
  node[:php][:ini][:zend] ||= {}
  node[:php][:ini][:windows] ||= {}
  node[:php][:ini][:arg_separator] ||= {}
  node[:php][:ini][:cgi] ||= {}
  node[:php][:ini][:fastcgi] ||= {}
  # [Session] Section
  node[:php][:ini][:url_rewriter] ||= {}

EOF


# [Section]		    -> node[:php][:ini][:Section] ||= {}
# section.key = "value"	    -> node[:php][:ini][:section][:key] ||= '"value"'
# ;section.key=commented    -> #node[:php][:ini][:section][:key] ||= 'commented'
egrep '^\[|[[:alnum:]_\.]+[[:space:]]*=' "$1" \
    | grep -v '^\[PHP\]' \
    | grep -v '^;.* Value:' \
    | grep -v '^;.* section' \
    | grep -v '^;.* [Dd]irective ' \
    | grep -v '^;.* session.save_path =' \
    | grep -v '^;.* foo =' \
    | grep -v '^;.* On =' \
    | grep -v '^;.* Off =' \
    | grep -v '^;.* Integer =' \
    | grep -v '^;.* Default =' \
    | grep -v '^;.* [-]*[0-9] =' \
    | grep -v '^;.* seconds =' \
    | grep -v '^;.* stdout =' \
    | grep -v '^;.* stderr =' \
    | sed -e 's/^;/#/' \
    | sed -e 's/^\[CLI Server]/[cli_server]/' \
    | sed -e 's/^\[Date]/[date]/' \
    | sed -e 's/^\[Pcre]/[pcre]/' \
    | sed -e 's/^\[Pdo/[pdo/' \
    | sed -e 's/^\[Phar]/[phar]/' \
    | sed -e 's/^\[mail function]/[mail]/' \
    | sed -e 's/^SMTP =/mail.SMTP =/' \
    | sed -e 's/^smtp_port =/mail.smtp_port =/' \
    | sed -e 's/^#sendmail_from =/#mail.sendmail_from =/' \
    | sed -e 's/^#sendmail_path =/#mail.sendmail_path =/' \
    | sed -e 's/^\[SQL]/[sql]/' \
    | sed -e 's/^\[ODBC]/[odbc]/' \
    | sed -e 's/^\[Interbase]/[ibase]/' \
    | sed -e 's/^\[MySQL/[mysql/' \
    | sed -e 's/^\[OCI8]/[oci8]/' \
    | sed -e 's/^\[PostgreSQL]/[pgsql]/' \
    | sed -e 's/^\[Sybase-CT]/[sybct]/' \
    | sed -e 's/^\[Session]/[session]/' \
    | sed -e 's/^\[MSSQL]/[mssql]/' \
    | sed -e 's/^\[Assertion]/[assert]/' \
    | sed -e 's/^\[COM]/[com]/' \
    | sed -e 's/^\[Tidy]/[tidy]/' \
    | sed -e "s/[[:space:]]*; [^\"']*$//" \
    | sed -e 's/\([[:alnum:]_\.]*\)[[:space:]]*=/node[:php][:ini][:\1] ||=/' \
    | sed -e "s/=[[:space:]]*\\(.*\\)$/= \'\\1\'/" \
    | sed -e 's/^\(#*[[:space:]]*\)node\[:php]\[:ini]\[:\([[:alnum:]_]*\)\.\([[:alnum:]_]*\)]/\1node[:php][:ini][:\2][:\3]/' \
    | sed -e 's/^\[\([[:print:]]*\)]/node[:php][:ini][:\1] ||= {}/' \
    | sed -e 's/^/  /'

echo "end"

