#
# Generate and put php.ini
#

include_recipe 'attributes'

if defined? node[:php][:system][:ini][:path] and !node[:php][:system][:ini][:path].nil? and defined? node[:php][:ini] and node[:php][:ini].kind_of? Hash

  file node[:php][:system][:ini][:path] do

    php_ini  = "; Generated php.ini file\n"
    php_ini += "[PHP]\n\n"
    php_ini_sections = ''
    php_ini_section_session = ''

    node[:php][:ini].each do |section, directives|
      unless directives.kind_of? Hash
        # "engine = On"
        php_ini += "#{section} = #{directives}\n"
        next
      end

      section_name = section
      case section
        # [PHP] section
      when 'user_ini', 'zlib', 'highliht', 'zend', 'windows', 'arg_separator', 'cgi', 'fastcgi' then
        directives.each do |directive, value|
          php_ini += "#{section}.#{directive} = #{value}\n"
        end
        next

      when 'url_rewriter' then
        # [Session] section
        directives.each do |directive, value|
          php_ini_section_session = "#{section}.#{directive} = #{value}\n"
        end
        next

      when 'cli_server' then
        # Rename
        section_name = 'CLI Server'
      when 'date' then
        section_name = 'Date'
      when 'pcre' then
        section_name = 'Pcre'
      when 'pdo' then
        section_name = 'Pdo'
      when 'pdo_mysql' then
        section_name = 'Pdo_mysql'
      when 'phar' then
        section_name = 'Phar'
      when 'mail' then
        section_name = 'mail function'
      when 'sql' then
        section_name = 'SQL'
      when 'odbc' then
        section_name = 'ODBC'
      when 'ibase' then
        section_name = 'Interbase'
      when 'mysql' then
        section_name = 'MySQL'
      when 'mysqli' then
        section_name = 'MySQLi'
      when 'oci8' then
        section_name = 'OCI8'
      when 'pgsql' then
        section_name = 'PostgreSQL'
      when 'sybct' then
        section_name = 'Sybase-CT'
      when 'session' then
        section_name = 'Session'
      when 'mssql' then
        section_name = 'MSSQL'
      when 'assert' then
        section_name = 'Assertion'
      when 'com' then
        section_name = 'COM'
      when 'tidy' then
        section_name = 'Tidy'
      end
      php_ini_sections += "\n[#{section_name}]\n\n"

      directives.each do |directive, value|
        case directive
        when 'SMTP', 'smtp_port', 'sendmail_from', 'sendmail_path' then
          php_ini_sections += "#{directive} = #{value}\n"
        else
          php_ini_sections += "#{section}.#{directive} = #{value}\n"
        end
      end
      php_ini_sections += php_ini_section_session if section == 'session'

    end

    php_ini += php_ini_sections

    content php_ini
    owner node[:php][:system][:ini][:owner]
    group node[:php][:system][:ini][:group]
    mode node[:php][:system][:ini][:mode]
    path node[:php][:system][:ini][:path]
  end
end

