#
# ディストリビューションが提供する最低限の PHP システムをインストールする。
#



include_recipe "../apache/default.rb"

package "Install Basic PHP packages" do
  action :install

  # serverspec でもプラットフォーム別のパッケージ名の抽象化は
  # できないようなので、振り分けを行う。
  case node['platform']
  when 'redhat', 'amazon' then
    # php-common は依存関係でインストールされる。
    name 'php php-cli php-mbstring'
  end
end

# Install php.ini
include_recipe 'php_ini'

service 'Restart Apache HTTP daemon' do
  action :restart
  name node[:apache][:system][:service]
end

