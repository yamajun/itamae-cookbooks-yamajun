#
# UNIX システム共通のユーザー作成処理を定義する
#

# 下記フォーマットの YAML, JSON ファイルで作成するユーザーIDを設定し、
# 起動時に --node-yaml 設定.yaml または --node-json 設定.json
# オプションをつけて起動する。
#
# users:
#   - user: user1
#     uid: 1234
#     gid: wheel
#     # /home/sample が作成されていないとエラーになる
#     home: /home/sample/user1
#     # パスワードにはハッシュ化済み文字列を設定する必要がある
#     password: sAmple_p@ssW0rD
#     initial_ssh_pubkey: |
#       # SSH public keys
#       ssh-ed25519 AAAA...
#       ssh-rsa AAAA...
#   - user: user2
#   - user: user3

# ユーザー作成処理の定義
define :create_unix_user,
          uid: nil,
          gid: nil,
          home: nil,
          password: nil,
          initial_ssh_pubkey: nil do
  username  = params[:name] # ユーザーID
  uid       = params[:uid]  # 番号の方
  gid       = params[:gid]  # gid が nil の時には gid == username
  home      = params[:home]
  password  = params[:password]

  user 'Create user' do
    action :create

    username  username
    uid       uid       unless uid.nil?
    gid       gid       unless gid.nil?
    password  password  unless password.nil?
    home      home      unless home.nil?
    create_home true
  end

  # Create $HOME/.ssh
  # resource-authorized_keys プラグインがあるなら本来不要だけど、
  # サンプルとして書いておく。
  directory '.ssh' do
    action :create
    user username
  end

  unless params[:initial_ssh_pubkey].nil?
    # itamae-plugin-resource-authorized_keys
    # https://github.com/nownabe/itamae-plugin-resource-authorized_keys
    # プラグインが必要。
    # $ gem install --user-install itamae-plugin-resource-authorized_keys
    require 'itamae/plugin/resource/authorized_keys'

    initial_ssh_pubkey = params[:initial_ssh_pubkey]
    authorized_keys username do
      owner username
      # authorized_keys のブロックの中では
      # content params[:initial_ssh_pubkey] はエラーになる。
      content initial_ssh_pubkey
    end
  end
end


# 実際のユーザー作成処理
for user_node in node[:users]
  create_unix_user user_node[:user] do
    uid       user_node[:uid]
    gid       user_node[:gid]
    home      user_node[:home]
    password  user_node[:password]
    initial_ssh_pubkey user_node[:initial_ssh_pubkey]
  end
end

