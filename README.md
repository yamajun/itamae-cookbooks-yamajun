
公開 itamae cookbook
======================

How to install itamae
---------------------

[itamae](https://github.com/itamae-kitchen/itamae/) の Rubygem を
インストールする手順

    $ gem install --user-install itamae
    または
    $ sudo gem install itamae


この cookbook でユーザー作成後に `authorized_keys` ファイルの配布を行うには、
`resource-authorized_keys` プラグインが必要なので、ユーザーの作成作業も行うならインストールする。

    $ gem install --user-install itamae-plugin-resource-authorized_keys


Usage
-----

Local

    $ itamae local cookbook/apache/default.rb

via SSH

    $ itamae ssh -u USER -h HOSTNAME cookbook/apache/default.rb

via "vagrant ssh"

    $ cd /path/to/vagrant/vm
    $ vagrant up
    $ itamae ssh --vagrant cookbook/apache/default.rb

実行例

    $ cat > hello_itamae.rb
    execute "echo 'Hello world'" do
      action :run
    end
    ^D
    $ /home/yamajun/.gem/ruby/2.0.0/bin/itamae ssh --no-sudo --host ::1 hello_itamae.rb
    Text will be echoed in the clear. Please install the HighLine or Termios libraries to suppress echoed text.
    yamajun@::1's password:
    (この状態でパスワードを入力すると、入力されたパスワードが表示されるので、
    何らかの手段で ruby-termios をインストールする必要がある)

    (下記のように直接gemをインストールするか、各OSのパッケージシステムを利用する)
    $ gem install --user-install ruby-termios


Warning
-------
itamae が依存している net-ssh が 4.0 にならないと、ed25519 な SSH 鍵は使えない。

[送別会, Ruby の net-ssh ではまだ ed25519 が使えない - HsbtDiary(2016-03-24)](https://www.hsbt.org/diary/20160324.html#p02)
> ちなみに ed25519 に対応するための pull request は出ていて...
> 
> https://github.com/net-ssh/net-ssh/pull/228
> 
> って確認したらマージされていた。4.0.0 では使えるようになるらしいです。


Refarence
---------

[itamaeに入門してDocker調理してみた](http://www.slideshare.net/NaokiIshibashi/itamaedocker)
より
> つまり
> * どんなディレクトリ構成にしようと
> * 長大なレシピを作ろうと
> * 環境ごとにレシピを作って全然dryじゃなくなろうと
> 
> 自由だ～！！
> 
> それはヤバイので
> * ベストプラクティス見ましょう
> * https://github.com/itamae-kitchen/itamae/wiki/Best-Practice

