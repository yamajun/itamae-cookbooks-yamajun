# see .gem/ruby/2.0.0/gems/itamae-1.9.8/lib/itamae/resource/package.rb
#

require 'itamae/resource/base'

module Itamae
  module Plugin
    module Resource
      class Pear < Itamae::Resource::Base
        define_attribute :action, default: :install
        define_attribute :name, type: String, default_name: true
        #define_attribute :version, type: String
        #define_attribute :options, type: String

        #def pre_action
        #  case @current_action
        #  when :install
        #    attributes.installed = true
        #  when :remove
        #    attributes.installed = false
        #  end
        #end

        #def set_current_attributes
        #end

        def action_install(options)
          # pear "first second" -> "pear info first", "pear info second"
          attributes.name.split(/\s+/).each do |pear_package|
            unless run_command("pear info #{pear_package}", error: false).exit_status == 0
              run_command(["pear",  "install", pear_package])
              updated!
            end
          end
        end

        def action_remove(options)
          # pear "first second" -> "pear info first", "pear info second"
          attributes.name.split(/\s+/).each do |pear_package|
            if run_command("pear info #{pear_package}", error: false).exit_status == 0
              run_command(["pear",  "uninstall", pear_package])
              updated!
            end
          end
        end

      end
    end
  end
end

